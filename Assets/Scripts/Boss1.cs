﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Controller2DBoss))]
public class Boss1 : MonoBehaviour {

    public float maxJumpHeight = 4;
    public float minJumpHeight = 1;
    public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    float moveSpeed = 6;

    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;

    public float wallSlideSpeedMax = 3;
    public float wallStickTime = .25f;
    float timeToWallUnstick;

    public float gravity;
    float maxJumpVelocity;
    float minJumpVelocity;
    Vector3 velocity;
    float velocityXSmoothing;

    Controller2DBoss controller;

    Vector2 directionalInput;
    bool wallSliding;
    int wallDirX;

    public bool isFacingRight;

    public int lap;

    void Start()
    {
        controller = GetComponent<Controller2DBoss>();

        //gravity = -(2 * maxJumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        minJumpVelocity = Mathf.Sqrt(2 * Mathf.Abs(gravity) * minJumpHeight);
        isFacingRight = true;
        lap = 0;

        NotificationCenter.DefaultCenter().AddObserver(this, "Hit");
        NotificationCenter.DefaultCenter().AddObserver(this, "Trap");
    }

    void Update()
    {
        CalculateVelocity();
        //HandleWallSliding ();
        controller.getDirectionToTarget();
        controller.Move(velocity * Time.deltaTime, directionalInput, isFacingRight);

        if (controller.collisions.above || controller.collisions.below)
        {
            if (controller.collisions.slidingDownMaxSlope)
            {
                velocity.y += controller.collisions.slopeNormal.y * -gravity * Time.deltaTime;
            }
            else
            {
                velocity.y = 0;
            }
        }
        if (lap == 1)
        {
            UntilGrounded();
        }
        if (lap == 2)
        {
            FollowTarget(controller.getDirectionToTarget());
        }
    }

    public void SetDirectionalInput(Vector2 input)
    {
        if (directionalInput.x > 0 && !isFacingRight || directionalInput.x < 0 && isFacingRight)
        {
            isFacingRight = !isFacingRight;
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        }
        directionalInput = input;
    }

    public void OnJumpInputDown()
    {
        if (wallSliding)
        {
            if (wallDirX == directionalInput.x)
            {
                velocity.x = -wallDirX * wallJumpClimb.x;
                velocity.y = wallJumpClimb.y;
            }
            else if (directionalInput.x == 0)
            {
                velocity.x = -wallDirX * wallJumpOff.x;
                velocity.y = wallJumpOff.y;
            }
            else
            {
                velocity.x = -wallDirX * wallLeap.x;
                velocity.y = wallLeap.y;
            }
        }
        if (controller.collisions.below)
        {
            if (controller.collisions.slidingDownMaxSlope)
            {
                if (directionalInput.x != -Mathf.Sign(controller.collisions.slopeNormal.x))
                { // not jumping against max slope
                    velocity.y = maxJumpVelocity * controller.collisions.slopeNormal.y;
                    velocity.x = maxJumpVelocity * controller.collisions.slopeNormal.x;
                }
            }
            else
            {
                velocity.y = maxJumpVelocity;
            }
        }
    }

    public void OnJumpInputUp()
    {
        if (velocity.y > minJumpVelocity)
        {
            velocity.y = minJumpVelocity;
        }
    }


    void HandleWallSliding()
    {
        wallDirX = (controller.collisions.left) ? -1 : 1;
        wallSliding = false;
        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {
            wallSliding = true;

            if (velocity.y < -wallSlideSpeedMax)
            {
                velocity.y = -wallSlideSpeedMax;
            }

            if (timeToWallUnstick > 0)
            {
                velocityXSmoothing = 0;
                velocity.x = 0;

                if (directionalInput.x != wallDirX && directionalInput.x != 0)
                {
                    timeToWallUnstick -= Time.deltaTime;
                }
                else
                {
                    timeToWallUnstick = wallStickTime;
                }
            }
            else
            {
                timeToWallUnstick = wallStickTime;
            }

        }

    }

    void CalculateVelocity()
    {
        float targetVelocityX = directionalInput.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
    }

    private void FollowTarget(Vector2 direction)
    {
        float targetVelocityX = direction.x * moveSpeed;
        if (Mathf.Abs(direction.x - velocity.x) > 0.09f && controller.collisions.above)
        {
            //Debug.Log(direction.x - velocity.x);
            velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
            //velocity.y += gravity * Time.deltaTime;
        }
        /*else{
            //Debug.Log("WTF!!  " + Mathf.Abs(direction.x - velocity.x));
            lap = 0;
            gravity = gravity * -1;
        }*/
    }

    void Hit(Notification notification)
    {
        if(lap == 0)
        {
            print("Oh God!! someone hit me: " + (string)notification.data);
            lap = 1;
        }
        if (lap == 1)
        {
            if (gravity < 0)
            {
                gravity = gravity * -1;
            }
        }
    }

    void Trap(Notification notification)
    {
        if(lap > 0)
        {
            gravity = gravity * -1;
            lap = 0;
        }
    }

    private void UntilGrounded()
    {
        //Debug.Log(controller.collisions.above);
        if (controller.collisions.above)
        {
            lap = 2;
        }
    }
}
